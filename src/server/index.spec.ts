const appMock = {
    listen: jest.fn((_port, cb) => cb())
}

jest.mock("./app", () => ({
    __esModule: true,
    app: appMock
}));

describe("Index", () => {
    test("should start server", () => {
        // @ts-ignore
        const index = require(".");
        expect(appMock.listen).toHaveBeenCalled();
    })
})