import { format, createLogger, transports } from "winston";

const { printf, combine, timestamp, json } = format;
const { Console } = transports;

const level = process.env.LOG_LEVEL || "debug";

const toString = printf(info => {
    return `${info.timestamp} - ${info.level}: ${info.message}`;
});

export const logger = createLogger({
    format: combine(
        timestamp(),
        json()
    ),
    transports: [
        new Console({
            level,
            format: toString
        })
    ]
});

