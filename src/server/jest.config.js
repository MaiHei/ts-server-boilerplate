// Configuration for test runs.
// Supress logging
process.env.LOG_LEVEL = "trace";
process.env.DEPLOYMENT = "DEVELOPMENT"

module.exports = {
  "transform": {
    "^.+\\.tsx?$": "ts-jest"
  },
  "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.(tsx?)$",
  "moduleFileExtensions": [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node"
  ],
  "collectCoverageFrom": [
    "**/*.{ts,tsx}",
    "!dist/**/*",
    "!**/*.d.ts",
    "!routes.ts",
    "!utils/testing.ts"
  ],
  "setupFiles": [
    "./jest.config.js"
  ],
  "coverageThreshold": {
    "global": {
      "branches": 85,
      "functions": 85,
      "lines": 85,
      "statements": 85
    }
  },
  "globals": {
    "ts-jest": {
      "ignoreCoverageForDecorators": true,
      "ignoreCoverageForAllDecorators": true
    }
  }
}