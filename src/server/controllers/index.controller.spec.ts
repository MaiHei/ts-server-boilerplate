import { IndexController } from "./index.controller"

describe("Index controller", () => {
    let testable: IndexController;

    beforeEach(() => {
        testable = new IndexController();
    })

    test("should return hello world", () => {
        expect(testable.index()).toEqual("Hello world");
    })
})