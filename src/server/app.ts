import express from "express";
import bodyParser from "body-parser";
import { RegisterRoutes } from "./routes";
import { swaggerHtmlMiddleware, swaggerJsonMiddleware } from "./middleware/swagger.middleware";

export const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

RegisterRoutes(app);

app.use("/api/swagger", ...swaggerHtmlMiddleware);
app.use("/api", swaggerJsonMiddleware);