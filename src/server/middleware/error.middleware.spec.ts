import { errorMiddleware } from "./error.middleware";
import { ApiError } from "../models/ApiError";
import { mockNext, mockReq, mockRes } from "../utils/testing";

describe("Error Middleware", () => {

    it("can handle objects as errors", async () => {
        const req = mockReq();
        const res = mockRes();
        const nextFn = mockNext();
        const error = { message: "Error" };
        errorMiddleware(error, req, res, nextFn);
        expect(res.status).toHaveBeenCalledWith(500);
        expect(res.send).toHaveBeenCalledWith({
            error,
            message: error.message
        });
    });

    it("can handle objects as errors with status set", async () => {
        const req = mockReq();
        const res = mockRes();
        const nextFn = mockNext();
        const error = { message: "Error", status: 401 };
        errorMiddleware(error, req, res, nextFn);
        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.send).toHaveBeenCalledWith({
            error,
            message: error.message
        });
    });

    it("can handle objects as errors with statusCode set", async () => {
        const req = mockReq();
        const res = mockRes();
        const nextFn = mockNext();
        const error = { name: "Error message as name", statusCode: 401 };
        errorMiddleware(error, req, res, nextFn);
        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.send).toHaveBeenCalledWith({
            error,
            message: error.name
        });
    });

    it("can handle ApiError objects and extract statusCode", async () => {
        const req = mockReq();
        const res = mockRes();
        const nextFn = mockNext();
        const error = new ApiError("test error", 401);
        errorMiddleware(error, req, res, nextFn);
        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.send).toHaveBeenCalledWith({
            error,
            message: error.name
        });
    });
})