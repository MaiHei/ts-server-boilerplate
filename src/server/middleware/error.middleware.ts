import { ErrorRequestHandler, Request, Response } from "express";
import { VError } from "verror";
import { logger } from "../logging/logging";
import isDevelopment from "../utils/isDevelopment";

let getError = (_err: any) => ({});

if (isDevelopment()) {
    getError = (err) => err;
}

function middleware(err: any, _req: Request, res: Response) {
    let forLog = err;
    if (!(forLog instanceof Error)) {
        forLog = new Error(JSON.stringify(err));
        if (err.status || err.statusCode) {
            forLog.status = err.status || err.statusCode;
        }
    }
    logger.error(VError.fullStack(forLog));
    res.status(err.status || err.statusCode || 500).send({
        error: getError(err),
        message: err.message || err.name,
    });
}

export const errorMiddleware: ErrorRequestHandler = middleware;
