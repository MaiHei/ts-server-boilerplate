import { resolve } from "path";
import { serve, setup } from "swagger-ui-express";
import { readFileSync } from "fs";
import { Request, Response } from "express";

const swaggerDoc = resolve(__dirname, "..", "..", "..", "docs", "swagger.json");

export function swaggerJsonMiddleware(_req: Request, res: Response) {
    res.sendFile(swaggerDoc);
}

export const swaggerHtmlMiddleware = [serve, setup(readFileSync(swaggerDoc))]