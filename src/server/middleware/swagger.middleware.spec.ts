import { mockRes, mockReq } from "../utils/testing";
import { swaggerHtmlMiddleware, swaggerJsonMiddleware } from "./swagger.middleware";
import * as fs from "fs";

jest.mock("fs");

describe("swaggerMiddleware", () => {
    it("will send swagger.json", async () => {
        const res = mockRes();
        swaggerJsonMiddleware(mockReq(), res);
        expect(res.sendFile).toHaveBeenCalled();
    });

    it("will serve swaggerUI", async () => {
        const testable = swaggerHtmlMiddleware;
        expect(testable).toHaveLength(2);
        expect(fs.readFileSync).toHaveBeenCalled();
    });
});
